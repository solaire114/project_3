/*
 * catsem.c
 *
 * 30-1-2003 : GWA : Stub functions created for CS161 Asst1.
 *
 * NB: Please use SEMAPHORES to solve the cat syncronization problem in 
 * this file.
 */


/*
 * 
 * Includes
 *
 */

#include <types.h>
#include <lib.h>
#include <test.h>
#include <thread.h>
#include <synch.h>


// Semaphores
volatile int all_dishes_available = 1;
struct semaphore *done;
struct semaphore *mutex;
struct semaphore *dish_mutex;
struct semaphore *cats_queue;
volatile int cats_wait_count = 0;
volatile int no_cat_eat = 1; //first cat
struct semaphore *mice_queue;
volatile int mice_wait_count = 0;
volatile int no_mouse_eat = 1;

struct semaphore *cat_mutex;
struct semaphore *mouse_mutex;
volatile int cats_done = 0;
volatile int mouse_done = 0;
volatile int dish1_busy = 0;
volatile int dish2_busy = 0;



/*
 * 
 * Constants
 *
 */

/*
 * Number of food bowls.
 */

#define NFOODBOWLS 2

/*
 * Number of cats.
 */

#define NCATS 6

/*
 * Number of mice.
 */

#define NMICE 2


/*
 * 
 * Function Definitions
 * 
 */


/*
 * catsem()
 *
 * Arguments:
 *      void * unusedpointer: currently unused.
 *      unsigned long catnumber: holds the cat identifier from 0 to NCATS - 1.
 *
 * Returns:
 *      nothing.
 *
 * Notes:
 *      Write and comment this function using semaphores.
 *
 */

static
void
catsem(void * unusedpointer, 
		unsigned long catnumber)
{
	/*
	 * Avoid unused variable warnings.
	 */

	(void) unusedpointer;
	(int) catnumber; //Cast catnumber to an int for future use
	catnumber++;

	// Loops the process (in order to ensure that there is no starvation past the first cycle)
	while (1==1) {
		int my_dish = 0;

		// Increment the cat_wait_count, showing that there is a cat in need of food in the queue
		P(cat_mutex);
		cats_wait_count++;
		V(cat_mutex);

		// If there are no mice waiting, and there are at least 2 cats waiting, send in two cats
		if (mice_wait_count < 1 && cats_wait_count < 2) {
			V(cats_queue);
			V(cats_queue);
		}

		P(cats_queue);
		P(mutex);

		// Assign a cat a dish (depending on what dish is available)
		if(dish1_busy == 0) {
			dish1_busy = 1;
			my_dish = 1;
		} else {
			dish2_busy = 1;
			my_dish = 2;
		}

		kprintf("\ncat %d is eating from dish %d", catnumber, my_dish);
		V(mutex);
		clocksleep(1);

		// Increment cats_done so that the process knows the total number of cats that have eaten
		P(cat_mutex);
		cats_done++;
		cats_wait_count--;

		// Release the dish that was in use by the cat (That is now done eating)
		if (my_dish == 1) {
			dish1_busy = 0;
		} else {
			dish2_busy = 0;
		}

		// If multiple cats have gone ,ensure that the mice have a turn.
		// If no mice are waiting, then continue sending in cats
		if (cats_done == 2) {
			V(cat_mutex);
			P(mutex);
			kprintf("\ncat %d left the kitchen", catnumber);
			V(mutex);
			if (mice_wait_count > 0) {
				V(mice_queue);
				V(mice_queue);
				P(cat_mutex);
				cats_done = 0;
				V(cat_mutex);


			} else if(cats_wait_count > 0) {
				P(cat_mutex);
				cats_done = 0;
				V(cat_mutex);
				V(cats_queue);
				V(cats_queue);


			}
		} else {
			V(cat_mutex);
			P(mutex);
			kprintf("\ncat %d left the kitchen", catnumber);
			V(mutex);

		}

	}

}


/*
 * mousesem()
 *
 * Arguments:
 *      void * unusedpointer: currently unused.
 *      unsigned long mousenumber: holds the mouse identifier from 0 to 
 *              NMICE - 1.
 *
 * Returns:
 *      nothing.
 *
 * Notes:
 *      Write and comment this function using semaphores.
 *
 */

static
	void
mousesem(void * unusedpointer, 
		unsigned long mousenumber)
{
	/*
	 * Avoid unused variable warnings.
	 */


	(void) unusedpointer;
	(int) mousenumber; //Cast catnumber to an int for future use
	mousenumber++;

	// Loops the process (in order to ensure that there is no starvation past the first cycle)
	while (1==1) {
		int my_dish = 0;

		// Increment the mice_wait_count, showing that there is a mouse in need of food in the queue
		P(mouse_mutex);
		mice_wait_count++;
		V(mouse_mutex);

		// If there are no cats waiting, but there is a mouse waiting, send in the additional mouse
		if (cats_wait_count < 1 && mice_wait_count < 2) {
			V(mice_queue);
		}

		P(mice_queue);
		P(mutex);

		//Assign a mouse a dish (depending on what dish is available)
		if(dish1_busy == 0) {
			dish1_busy = 1;
			my_dish = 1;
		} else {
			dish2_busy = 1;
			my_dish = 2;
		}

		kprintf("\nmouse %d is eating from dish %d", mousenumber, my_dish);
		V(mutex);
		clocksleep(1);

		// Increment mouse_done so that the process knows the total number of mice that have eaten
		P(mouse_mutex);
		mouse_done++;
		mice_wait_count--;

		// Release the dish that was in use by the mouse (That is now done eating)
		if (my_dish == 1) {
			dish1_busy = 0;
		} else {
			dish2_busy = 0;
		}

		// If both mice have eaten, they leave and call a cat in 
		if (mouse_done == 2) {
			mouse_done = 0;
			V(mouse_mutex);
			P(mutex);
			kprintf("\nmouse %d left the kitchen", mousenumber);
			V(mutex);
			V(cats_queue);
			V(cats_queue);

		} 

		else {
			V(mouse_mutex);
			P(mutex);
			kprintf("\nmouse %d left the kitchen", mousenumber);
			V(mutex);
		}		

	}

}


/*
 * catmousesem()
 *
 * Arguments:
 *      int nargs: unused.
 *      char ** args: unused.
 *
 * Returns:
 *      0 on success.
 *
 * Notes:
 *      Driver code to start up catsem() and mousesem() threads.  Change this 
 *      code as necessary for your solution.
 */

	int
catmousesem(int nargs,
		char ** args)
{

	// Instantiate semaphores for the processes
	done = sem_create("done", 0);
	mutex = sem_create("mutex", 1);
	dish_mutex = sem_create("dish_mutex", 1);
	cats_queue = sem_create("cats_queue", 0);
	mice_queue = sem_create("mice_queue", 0);
	cat_mutex = sem_create("cat_mutex", 1);
	mouse_mutex = sem_create("mouse_mutex", 1);




	int index, error;

	/*
	 * Avoid unused variable warnings.
	 */

	(void) nargs;
	(void) args;

	/*
	 * Start NCATS catsem() threads.
	 */

	for (index = 0; index < NCATS; index++) {

		error = thread_fork("catsem Thread", 
				NULL, 
				index, 
				catsem, 
				NULL
				);

		/*
		 * panic() on error.
		 */

		if (error) {

			panic("catsem: thread_fork failed: %s\n", 
					strerror(error)
			     );
		}
	}

	/*
	 * Start NMICE mousesem() threads.
	 */

	for (index = 0; index < NMICE; index++) {

		error = thread_fork("mousesem Thread", 
				NULL, 
				index, 
				mousesem, 
				NULL
				);

		/*
		 * panic() on error.
		 */

		if (error) {

			panic("mousesem: thread_fork failed: %s\n", 
					strerror(error)
			     );
		}
	}

	return 0;
}


/*
 * End of catsem.c
 */
