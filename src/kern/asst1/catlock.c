/*
 * catlock.c
 *
 * 30-1-2003 : GWA : Stub functions created for CS161 Asst1.
 *
 * NB: Please use LOCKS/CV'S to solve the cat syncronization problem in 
 * this file.
 */


/*
 * 
 * Includes
 *
 */

#include <types.h>
#include <lib.h>
#include <test.h>
#include <thread.h>
#include <synch.h>


/*
 * 
 * Constants
 *
 */

/*
 * Number of food bowls.
 */

#define NFOODBOWLS 2

/*
 * Number of cats.
 */

#define NCATS 6

/*
 * Number of mice.
 */

#define NMICE 2

/*
 * 
 * Global Variables
 */
static struct lock *mutex;
static struct lock *drivermutex;
static struct cv *turn_cv;

//0 = No cat/mouse, 1 = cat, 2 = mouse
static volatile int turn_type;
static int volatile cats_wait_count;
static int volatile mice_wait_count;
static int volatile cats_in_this_turn;
static int volatile mice_in_this_turn;
static int volatile cats_eat_count;
static int volatile mice_eat_count;

//Dish busy (0,1)
static int volatile dish1_busy;
static int volatile dish2_busy;

//Tracks number of animals that have eaten
static int volatile total_count;



/*
 * 
 * Function Definitions
 * 
 */


/*
 * catlock()
 *
 * Arguments:
 *      void * unusedpointer: currently unused.
 *      unsigned long catnumber: holds the cat identifier from 0 to NCATS -
 *      1.
 *
 * Returns:
 *      nothing.
 *
 * Notes:
 *      Write and comment this function using locks/cv's.
 *
 */

static
	void
catlock(void * unusedpointer, 
		unsigned long catnumber)
{
	/*
	 * Avoid unused variable warnings.
	 */

	(void) unusedpointer;
	(int) catnumber; //Casts catnumber as an int to be used later
	catnumber++; //Increments catnumber so that it shows the proper number

	while(1 == 1) {
		int my_dish = 0;

		// Acquire the mutex and wait a cat
		lock_acquire(mutex);
		cats_wait_count++;

		//If no one has claimed the bowls, take them
		if(turn_type == 0)
		{
			turn_type = 1;
			cats_in_this_turn = 2;
		}
		//If the mice are eating at the bowls, wait
		while(turn_type == 2 || cats_in_this_turn == 0)
		{
			cv_wait(turn_cv, mutex);
		}

		cats_in_this_turn--;
		cats_eat_count++;

		//Claim the dish
		if(dish1_busy == 0)
		{
			dish1_busy = 1;
			my_dish = 1;
		}
		else
		{
			dish2_busy = 1;
			my_dish = 2;
		}

		kprintf("Cat %d is eating from dish %d.\n", catnumber, my_dish);

		lock_release(mutex);

		//Eat
		clocksleep(1);

		lock_acquire(mutex);

		kprintf("Cat %d is finished eating from dish %d.\n", catnumber, my_dish);

		//Release your dish
		if(my_dish == 1)
		{
			dish1_busy = 0;
		}
		else
		{
			dish2_busy = 0;
		}

		cats_eat_count --;
		cats_wait_count--;

		//If mice are waiting after 2 cats ate, let the mice eat
		if(mice_wait_count > 0 && cats_eat_count == 0)
		{
			mice_in_this_turn = 2;
			turn_type = 2;
			total_count++;
			cv_broadcast(turn_cv, mutex);
			lock_release(mutex);
		}
		else if(cats_wait_count > 0 && cats_eat_count == 0)
		{
			cats_in_this_turn = 2;
			total_count++;
			cv_broadcast(turn_cv, mutex);
			lock_release(mutex);
		}
		else if(cats_wait_count == 0 && mice_wait_count == 0)
		{
			turn_type = 0;
			total_count++;
			cv_signal(turn_cv, mutex);
			lock_release(mutex);
		}
		else
		{
			total_count++;
			lock_release(mutex);
		}

		if (total_count == 8) {
			total_count = 0;
		}
		while(total_count != 0) {}
	}

}


/*
 * mouselock()
 *
 * Arguments:
 *      void * unusedpointer: currently unused.
 *      unsigned long mousenumber: holds the mouse identifier from 0 to 
 *              NMICE - 1.
 *
 * Returns:
 *      nothing.
 *
 * Notes:
 *      Write and comment this function using locks/cv's.
 *
 */

static
	void
mouselock(void * unusedpointer,
		unsigned long mousenumber)
{
	/*
	 * Avoid unused variable warnings.
	 */

	(void) unusedpointer;
	(int) mousenumber;
	mousenumber++;
	//(void) mousenumber;

	//Dish used


	while(1==1) {
		int my_dish = 0;

		lock_acquire(mutex);
		mice_wait_count++;

		//If no one has claimed the bowls, take them
		if(turn_type == 0)
		{
			turn_type = 2;
			mice_in_this_turn = 2;
		}
		//If the cats are eating at the bowls, wait
		while(turn_type == 1 || mice_in_this_turn == 0)
		{
			cv_wait(turn_cv, mutex);
		}

		mice_in_this_turn--;
		mice_eat_count++;

		//Claim the dish
		if(dish1_busy == 0)
		{
			dish1_busy = 1;
			my_dish = 1;
		}
		else
		{
			dish2_busy = 1;
			my_dish = 2;
		}

		kprintf("Mouse %d is eating from dish %d.\n", mousenumber, my_dish);

		lock_release(mutex);

		//Eat
		clocksleep(1);

		lock_acquire(mutex);

		kprintf("Mouse %d is finished eating from dish %d.\n", mousenumber, my_dish);

		//Release your dish
		if(my_dish == 1)
		{
			dish1_busy = 0;
		}
		else
		{
			dish2_busy = 0;
		}

		mice_eat_count --;
		mice_wait_count--;

		//If cats are waiting after 2 mice ate, let the cats eat
		if(cats_wait_count > 0 && mice_eat_count == 0)
		{
			cats_in_this_turn = 2;
			turn_type = 1;
			total_count++;
			cv_broadcast(turn_cv, mutex);
			lock_release(mutex);
		}
		else if(mice_wait_count > 0 && mice_eat_count == 0)
		{
			mice_in_this_turn = 2;
			total_count++;
			cv_broadcast(turn_cv, mutex);
			lock_release(mutex);
		}
		else if(cats_wait_count == 0 && mice_wait_count == 0)
		{
			turn_type = 0;
			total_count++;
			cv_signal(turn_cv, mutex);
			mice_wait_count++;
			lock_release(mutex);
		}
		else
		{
			total_count++;
			lock_release(mutex);
		}
		
		if (total_count == 8) {
			total_count = 0;
		}
		while(total_count != 0) {}
	}

}


/*
 * catmouselock()
 *
 * Arguments:
 *      int nargs: unused.
 *      char ** args: unused.
 *
 * Returns:
 *      0 on success.
 *
 * Notes:
 *      Driver code to start up catlock() and mouselock() threads.  Change
 *      this code as necessary for your solution.
 */

	int
catmouselock(int nargs,
		char ** args)
{
	int index, error;

	//Initialize the locks and CVs
	mutex = lock_create("catlock and mouselock mutex");
	drivermutex = lock_create("driver mutex");
	turn_cv = cv_create("turn cv");

	//Initialize the global variables
	cats_wait_count = 0;
	mice_wait_count = 0;
	cats_in_this_turn = 0;
	mice_in_this_turn = 0;
	cats_eat_count = 0;
	mice_eat_count = 0;
	dish1_busy = 0;
	dish2_busy = 0;
	total_count = 0;

	/*
	 * Avoid unused variable warnings.
	 */

	(void) nargs;
	(void) args;

	/*
	 * Start NCATS catlock() threads.
	 */

	for (index = 0; index < NCATS; index++) {

		error = thread_fork("catlock thread", 
				NULL, 
				index, 
				catlock, 
				NULL
				);

		/*
		 * panic() on error.
		 */

		if (error) {

			panic("catlock: thread_fork failed: %s\n", 
					strerror(error)
			     );
		}
	}

	/*
	 * Start NMICE mouselock() threads.
	 */

	for (index = 0; index < NMICE; index++) {

		error = thread_fork("mouselock thread", 
				NULL, 
				index, 
				mouselock, 
				NULL
				);

		/*
		 * panic() on error.
		 */

		if (error) {

			panic("mouselock: thread_fork failed: %s\n", 
					strerror(error)
			     );
		}
	}

	return 0;
}

/*
 * End of catlock.c
 */
